const Reporting = require('perfecto-reporting');

const PerfectoUser = 'PerfectoUsername';
const PerfectoPass = 'PerfectoPassword';
const PerfectoSecurityToken = 'eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI5MWQ0NDlmZC00NzhkLTRhNDMtYWQ2My1kM2FhMjc1NDFmNGMiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTEwNzU4NDkxLCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2RlbW8tcGVyZmVjdG9tb2JpbGUtY29tIiwiYXVkIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJzdWIiOiI5YjI5ZmRlOS0xYzQzLTQxODctYjk2Zi00ZDNmODJkNzAxOWYiLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJzZXNzaW9uX3N0YXRlIjoiODgxNTdkOWItNWFjNC00Y2MwLWE4YmYtOTgyN2JkNDhjNmFmIiwiY2xpZW50X3Nlc3Npb24iOiJhNjFlNzFiNC1lZGQxLTRmOWMtODU2Zi1lODZkYzJkZWUxMWUiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50Iiwidmlldy1wcm9maWxlIl19fX0.Rs9LTGq0HC9Eex5RT2KcGHqolmedUclgbkdNjIOnUBTjx7X2yJ3E6ei388cGiizkGF0fORMMTfE88rKqvJ6WTxrYf5uy0g31tftzpdqS6CRwPONXA99GydOI9os24eEXyfxtHbWV39dyMsegK8ZUR6_-YQYJem3-2DATFn37N1gaG2bpIRWqk6fp6nusePU8E1w5ua6VPIvo_ktk2WfQl2kMKpULSfchhLSOa0H0EjSLB2udWwya3jdj3s7qzN4MCFUSG7qYi1Hd-kEIeBeTRM2nZyBqWfxCe3rnQFC7adD-B5Gpae3e5Amj7TcR8mghEbT4qjqGEjJcKQUurzcNig';
const PerfectoHost = 'demo.perfectomobile.com';

/**
 * Protractor setup
 *
 * @type {{seleniumAddress: string, capabilities: {browserName: string, user: string, password: string, platformName: string, model: string}, getPageTimeout: number, allScriptsTimeout: number, framework: string, frameworkPath: *, specs: string[], cucumberOpts: {require: string, tags: boolean, format: string, profile: boolean, no-source: boolean}}}
 */
exports.config = {

    /**
     * Remote Address
     */
//   seleniumAddress: 'http://' + PerfectoHost + '/nexperience/perfectomobile/wd/hub/fast',
    seleniumAddress: 'http://' + PerfectoHost + '/nexperience/perfectomobile/wd/hub',

    /**
     * Desired Capabilities
     * Set here your device capabilities.
     */
    multiCapabilities: [
        {
            browserName: 'chrome',
            securityToken: PerfectoSecurityToken,
            platformName: 'Android',
            deviceName: 'DAC64712'
        }, {
            securityToken: PerfectoSecurityToken,
            browserName: 'Safari',
            platformName : 'iOS',
            manufacturer : 'Apple',
            model : 'iPad Air 2',
            deviceName: '95E0EEA907ECC690914C7ABE68786B3EDE4343DF'
        },{
            securityToken: PerfectoSecurityToken,
            platformName : 'Windows',
            platformVersion : '10',
            browserName : 'Chrome',
            browserVersion : '67',
            resolution : '1280x1024',
            location : 'US East'
        }, {
            securityToken: PerfectoSecurityToken,
            platformName : 'Windows',
            platformVersion : '10',
            browserName : 'Chrome',
            browserVersion : 'beta',  // Beta Version of Chrome
            resolution : '1280x1024',
            location : 'US East'
        }, {
            securityToken: PerfectoSecurityToken,
            platformName : 'Windows',
            platformVersion : '10',
            browserName : 'Edge',
            browserVersion : '16',
            resolution : '1280x1024',
            location : 'US East'
        }, {
            securityToken: PerfectoSecurityToken,
            platformName : 'Windows',
            platformVersion : '10',
            browserName : 'Firefox',
            browserVersion : '59',
            resolution : '1280x1024',
            location : 'US East'
        // }, {
        //     securityToken: PerfectoSecurityToken,
        //     platformName : 'Mac',
        //     platformVersion : 'macOS High Sierra',
        //     browserName : 'Safari',
        //     browserVersion : '11',
        //     resolution : '1440x900',
        //     location : 'NA-US-BOS'
         }
    ],

    getPageTimeout: 60000,
    allScriptsTimeout: 500000,

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    // Spec patterns are relative to the current working directly when
    // protractor is called.
    specs: ['features/*.feature'],

    cucumberOpts: {
        require: 'features',
        tags: false,
        format: 'pretty',
        profile: false,
        'no-source': true
    },

    /**
     * A callback function called once protractor is ready and available, and
     * before the specs are executed.
     *
     * At this point browser instance is available.
     * Creating reporting client instance.
     * Reporting client is available using browser.reportingClient.
     */
    onPrepare: function () {

        browser.reportingClient = new Reporting.Perfecto.PerfectoReportingClient(
            new Reporting.Perfecto.PerfectoExecutionContext({
                webdriver: browser.driver,
                tags: ['davidde', 'protractor_cucumber', 'SE'] // optional
            }));



    }

};
