Feature: As a user I would like to navigate to google.
  Then I would like to search for Perfecto-Code repository in GitHub.
  After that I want to make sure that the page's title is what i expected for.

  @ScenarioTag
  Scenario: Search Angular.io page for testing.
    Given I navigate to "https://angular.io/"
    And I expect the title of the page to be "Angular"
    Then I search Angular site for "testing"
    And I see Angular Search Result "core/testing package "

  @ScenarioTag
  Scenario: Check Features page for 'Cross Platform'
    Given I navigate to "https://angular.io/"
    And I expect the title of the page to be "Angular"
    Then I select "Features" from the menu
    And I see "Cross Platform"