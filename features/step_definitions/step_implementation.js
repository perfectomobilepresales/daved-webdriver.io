/**
 * Steps implementation
 *
 * Implement here the scenario's steps.
 * Protractor browser is available at this point.
 */
const chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    STEP_TIMEOUT_SEC = 800 * 10000,
    EC = protractor.ExpectedConditions;

chai.use(chaiAsPromised);
expect = chai.expect;

module.exports = function () {
    this.Given(/^I'm on the google search page$/, {timeout: STEP_TIMEOUT_SEC}, () => {
        return browser.driver.get('http://www.google.com');
    });

    this.Then(/^I search for Perfecto-Code repository$/, {timeout: STEP_TIMEOUT_SEC}, () => {
        browser.driver.findElement(by.name('q'))
            .sendKeys('Perfecto-Code GitHub');

        return browser.driver.findElement(by.css('button.Tg7LZd,input[name=btnK]'))
            .click();
    });

    this.Then(/^click the first search result$/, {timeout: STEP_TIMEOUT_SEC}, () => {
        return browser.driver.findElement(by.partialLinkText('GitHub - PerfectoCode'))
            .click();
    });

    this.Then(/^I validate the page's title\.$/, {timeout: STEP_TIMEOUT_SEC}, ()=> {
        browser.driver.wait(() => {
            return browser.driver.getCurrentUrl()
                .then(url => url.indexOf('google') === -1);
        }, 9000)
            .then(() => expect(browser.driver.getTitle()).to.eventually.equal('GitHub - PerfectoCode/Samples: Product Samples'));
    });

    this.Given(/^I navigate to "([^"]*)"$/, {timeout: STEP_TIMEOUT_SEC}, function(site) {
        browser.get(site);
    });


    this.Then(/^I expect the title of the page to be "([^"]*)"$/, {timout: STEP_TIMEOUT_SEC}, function (title) {
        expect(browser.getTitle()).to.eventually.eql(title);
    });

    this.Then(/^I search Angular site for "([^"]*)"$/, {timeout: STEP_TIMEOUT_SEC}, (searchText) => {
        browser.driver.findElement(by.css('input[type="search"]'))
        .sendKeys(searchText);
    });

    //I see result "Test Code Page"
    this.Then(/^I see Angular Search Result "([^"]*)"$/, {timeout: STEP_TIMEOUT_SEC}, function (searchResult)  {
        expect(browser.driver.findElements(by.linkText(searchResult)));
        browser.driver.takeScreenshot();
    });


    this.Then(/^I select "([^"]*)" from the menu$/, {timeout: STEP_TIMEOUT_SEC}, function (menuTitle) {
        // Write code here that turns the phrase above into concrete actions
        var mob = '';
            browser.getProcessedConfig().then(function(cap) {
                mob = cap.capabilities.platformName.valueOf();
                console.log("--------------------------------");
                console.log(mob);
                console.log("--------------------------------");
                 if (mob === 'Android') {
                     var hamburgerMenu = element(by.css('button.hamburger.mat-button'));
                     hamburgerMenu.isDisplayed().then(function(visible){
                     if(visible){
                        hamburgerMenu.click();
                        var aboutBtn = element(by.css('aio-nav-menu.ng-star-inserted > aio-nav-item.ng-star-inserted > div.ng-star-inserted > button.vertical-menu-item.heading.level-1.collapsed.ng-star-inserted'));
                        aboutBtn.click();
                    }});
                 };
            });

         browser.driver.takeScreenshot();
         var lnkFeatures = element(by.xpath('//*[text()="' +  menuTitle + '" or text()=" '+ menuTitle + ' "]'));
         lnkFeatures.click();
    });

    this.Then(/^I see "([^"]*)"$/, function (text) {
        menuIsVisible = element(by.xpath("//*[contains(., ' " + text + " ') or contains(., '"+text+"')] "));
        browser.wait(menuIsVisible, 8000);
        browser.driver.takeScreenshot();
    });

};
